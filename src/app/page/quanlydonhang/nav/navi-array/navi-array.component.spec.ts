import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaviArrayComponent } from './navi-array.component';

describe('NaviArrayComponent', () => {
  let component: NaviArrayComponent;
  let fixture: ComponentFixture<NaviArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaviArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaviArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
