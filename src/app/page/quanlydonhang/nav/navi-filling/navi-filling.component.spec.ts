import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaviFillingComponent } from './navi-filling.component';

describe('NaviFillingComponent', () => {
  let component: NaviFillingComponent;
  let fixture: ComponentFixture<NaviFillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaviFillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaviFillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
