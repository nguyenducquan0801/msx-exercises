import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DropDownsModule, } from '@progress/kendo-angular-dropdowns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';




import { ContentComponent } from '../app/content/content/content.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';











@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    DateInputsModule,
    FormsModule,
    LabelModule,
    InputsModule,
    RouterModule.forChild(
      [{
          path: '',
          component: AppComponent
      }]
  ),
  CommonModule,
  InputsModule,
  NgxIntlTelInputModule,
  ReactiveFormsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
