import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { GroupDescriptor, DataResult, process } from '@progress/kendo-data-query';

@Component({
  selector: 'app-navi-filling',
  templateUrl: './navi-filling.component.html',
  styleUrls: ['./navi-filling.component.css']
})
export class NaviFillingComponent implements OnInit, OnChanges {
  @Input() customers = [];
  filterData = [];

  public gridView: DataResult;

  constructor() { }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.loadCustomers();
  }
  private loadCustomers(): void {
    this.gridView = process(this.filterData, {});
  }
  ngOnInit(): void {
    this.filterData = this.customers;
    this.loadCustomers();
  }
}
