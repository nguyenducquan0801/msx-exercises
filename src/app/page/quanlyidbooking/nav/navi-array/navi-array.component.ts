import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { GroupDescriptor, DataResult, process } from '@progress/kendo-data-query';
@Component({
  selector: 'app-navi-array',
  templateUrl: './navi-array.component.html',
  styleUrls: ['./navi-array.component.css']
})
export class NaviArrayComponent implements OnInit, OnChanges {
  @Input() customers = [];
  filterData = [];
  isActive = 'all';
  total = 100;

  public gridView: DataResult;
  public pageSize = 10;
  public skip = 0;

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadCustomers();
  }

  constructor() {
  }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.loadCustomers();
  }
  private loadCustomers(): void {
    this.gridView = process(this.filterData, {});
  }
  ngOnInit(): void {
    this.filterData = this.customers;
    this.loadCustomers();
  }

//Lọc
  filter(status: string) {
    this.isActive = status;
    if (status === 'all') {
      this.filterData = this.customers;
    } else {
      this.filterData = this.customers.filter(customer => customer.status === status);
    }
    
    this.loadCustomers();
  }
// Đếm
  count(status: string) {
    this.isActive = status;
    const list = this.customers.filter(customer => customer.status === status);
    return list ? list.length : 0;
  }
}

