import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
public listTour: Array<string> = ['SG-VT,VT-SG','VT-SG,SG-VT'];
public value: Date = new Date();

  constructor() { }
  ngOnInit(): void {
  }

}
