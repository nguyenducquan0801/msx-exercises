import { Component, OnInit } from '@angular/core';
import {customers} from '../../customers';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  public checked = true;

  customers = customers;
  
  public disable = true;
  constructor() { }

  ngOnInit(): void {
  }

}
