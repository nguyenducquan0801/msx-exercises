import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header/header.component';
import { SearchComponent } from './search/search/search.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { HttpClientModule } from '@angular/common/http';
import { IntlModule } from '@progress/kendo-angular-intl';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { NaviArrayComponent } from './nav/navi-array/navi-array.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { NaviFillingComponent } from './nav/navi-filling/navi-filling.component';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchComponent,
    NaviArrayComponent,
    NaviFillingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    DateInputsModule,
    GridModule,
    HttpClientModule,
    IntlModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
